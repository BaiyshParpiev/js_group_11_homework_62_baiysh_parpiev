import React, {useState} from 'react';
import './PortfolioBuilder.css'
import Carousel from "../Carousel/Carousel";
import mango from '../Carousel/images/CW-2.jpg'
import form from '../Carousel/images/form-desktop.jpg'
import temor from '../Carousel/images/indipixel.jpg'
import {Route} from "react-router-dom";
import Mango from "../Carousel/Mango/Mango";
import FormPage from "../Carousel/FormPage/FormPage";
import Temor from "../Carousel/Temor/Temor";



const PortfolioBuilder = ({history}) => {
    const openWork = e => {
        history.replace("/portfolio/" + e)
    }

    const back = () => {
        history.replace('/portfolio')
    }
    const [simple] = useState([
        {image: mango, button: 'Open', label: 'mango', instruments: 'HTML&CSS'},
        {image: form, button: 'Open', label: 'form', instruments: 'HTML&CSS'},
        {image: temor, button: 'Open', label: 'temor', instruments: 'HTML&CSS&SASS'},
    ])
    return (
        <>
            <Carousel content={simple} click={openWork}/>
            <Route path={'/portfolio/mango'} render={props=> (<Mango back={back} {...props}/>)}/>
            <Route path={'/portfolio/form'} render={props=> (<FormPage back={back} {...props}/>)}/>
            <Route path={'/portfolio/temor'} render={props=> (<Temor back={back} {...props}/>)}/>
        </>
    );
};

export default PortfolioBuilder;