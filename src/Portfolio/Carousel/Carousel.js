import Slider from 'react-animated-slider';
import horizontalCss from 'react-animated-slider/build/horizontal.css';
import './Carousel.css'
import React from "react";


const Carousel = ({content, click}) => {
    return (
        <div className="html-css">
            <Slider classNames={horizontalCss}>
                {content.map((item, index) => (
                    <div
                        key={index}
                        style={{
                            background: `url('${item.image}') no-repeat`,
                            backgroundSize: 'cover',
                        }}
                    >
                        <div className="center">
                            <button onClick={() => click(item.label)}>{item.button}</button>
                        </div>
                        <h4 className="info-inst">{item.instruments}</h4>
                    </div>
                ))}
            </Slider>
        </div>

    );
};

export default Carousel;