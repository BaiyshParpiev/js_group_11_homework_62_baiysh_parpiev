import React from 'react';
import logo from './images/logo.png';
import './style.css'
import companyFirst from './images/company-1.jpg';
import companySecond from './images/company-2.jpg';
import companyThird from './images/company-3.jpg';
import companyFourth from './images/company-4.jpg';
import anna from './images/anna.jpg';
import jhon from './images/john.jpg';



const Mango = ({back}) => (
    <>
        <header className="header">
            <div className="container header-inner">
                <div className="header-top">
                    <a href="#" className="logo">
                        <img src={logo} alt="MaguwoHost"/>
                    </a>
                    <nav className="main-nav ">
                        <ul>
                            <li><a href="#">About</a></li>
                            <li><a href="#">contacts</a></li>
                            <li><a href="#">Pricing</a></li>
                            <li><a href="#">Domain </a></li>
                            <li><a href="#">Hosting</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
        <div className="main-block">
            <div className="container">
                <h1>Just a Professional Web Hosting Services</h1>
                <div className="form">
                    <form>
                        <div className="form-row">
                            <input type="text" name="name" id="name" placeholder="Your Domain Name"/>
                            <button type="submit">Check Availability</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div className="block-services">
            <div className="container services-inner">
                <div className="wb-services services">
                    <h4>#1 Webhost Services</h4>
                    <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                        sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
                    </p>
                    <button type="button">More</button>
                </div>
                <div className="dm-services services">
                    <h4>#1 Domain Services</h4>
                    <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                        sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
                    </p>
                    <button type="button">More</button>
                </div>
            </div>
        </div>
        <div className="block-features">
            <div className="container">
                <h3>Our Features</h3>
                <h5>Our Best Features</h5>
                <div className="features-boxes">
                    <div className="box box-1">
                        <h5>Live Support</h5>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. amet mauris.
                            Morbi accumsan ipsum velit.</p>
                        <button type="button">Read more</button>
                    </div>
                    <div className="box box-2">
                        <h5>Cloud Technology</h5>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                        </p>
                        <button type="button">Read more</button>
                    </div>
                    <div className="box box-3">
                        <h5>Hi Tech Database</h5>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                        </p>
                        <button type="button">Read more</button>
                    </div>
                    <div className="box box-4">
                        <h5>Live Monitoring</h5>
                        <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.
                        </p>
                        <button type="button">Read more</button>
                    </div>
                </div>
            </div>
        </div>
        <div className="block-clients container">
            <div className="clients-inner">
                <div className="info">
                    <h3>Our Clients</h3>
                    <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                        sollicitudin, lorem quis bibendum.</p>
                </div>
                <div className="company-logos">
                    <a href="#"><img src={companyFirst} alt="logo-company"/></a>
                    <a href="#"><img src={companySecond} alt="logo-company"/></a>
                    <a href="#"><img src={companyThird} alt="logo-company"/></a>
                    <a href="#"><img src={companyFourth} alt="logo-company"/></a>
                </div>
            </div>
            <div className="clients-opinion">
                <div className="item">
                    <p>“ This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                        sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.
                        ”</p>
                    <div className="item-inner">
                        <span>-Anna</span>
                        <img src={anna} alt="girl" className="img"/>
                    </div>
                </div>
                <div className="item">
                    <p>“ This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean
                        sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum</p>
                    <div className="item-inner">
                        <span>-John Doe</span>
                        <img src={jhon} alt="boy" className="img"/>
                    </div>

                </div>
                <div className="item">
                    <p>“ Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit
                        consequat ipsum, nec sagittis sem nibh id elit. ”</p>
                    <div className="item-inner">
                        <span>-Anna</span>
                        <img src={anna} alt="girl" className="img"/>
                    </div>
                </div>
            </div>
        </div>
        <footer className="footer-mango">
            <div className="container">
                <p>Copyright 2014 Maguwohost by FikriStudio</p>
            </div>
        </footer>
        <button className="back" onClick={back}>Back</button>
    </>
);

export default Mango;