import React from 'react';
import './FormPage.css';
import logo from './images/logo.png'

const FormPage = ({back}) => (
    <>
        <header className="header-form">
            <div className="container header-inner">
                <div className="header-top">
                    <a href="#"><img src={logo} alt="logo"/></a>
                </div>
                <nav className="main-nav">
                    <ul>
                        <li><a href="#">Главная</a></li>
                        <li><a href="#">Фотографии</a></li>
                        <li><a href="#">Конкурс</a></li>
                    </ul>
                </nav>
            </div>
        </header>
        <div className="container content">
            <h1>Конкурс на путешествие с Пинк</h1>
            <p className="description">Поделитесь своей историей и получите шанс выиграть ценный приз — 1000 миль на
                вашу бонусную карту! Пожалуйста, заполните форму ниже:</p>
            <div className="form-block">
                <form>
                    <div className="form-section-1">
                        <div className="form-col-1">
                            <div className="form-row">
                                <label htmlFor="surName">Фамилия:</label>
                                <input type="text" name="surName" id="surName" placeholder="Укажите фамилию *" required autoFocus/>
                            </div>
                            <div className="form-row">
                                <label htmlFor="name">Имя:</label>
                                <input type="text" name="name" id="name" placeholder="Введите ваше имя *" required/>
                            </div>
                            <div className="form-row">
                                <label htmlFor="secondName">Отчество:</label>
                                <input type="text" name="secondName" id="secondName"
                                       placeholder="Укажите ваше отчество"/>
                            </div>
                        </div>
                        <div className="form-col-2">
                            <fieldset>
                                <legend>С каким приложением путешествовали?</legend>
                                <div className="form-row">
                                    <input type="radio" name="app" value="iOS" id="appIOS" className="app-field"/>
                                        <label htmlFor="appIOS" className="label-app">Pink для iOS</label>
                                </div>
                                <div className="form-row">
                                    <input type="radio" name="app" value="iOS" id="appAndroid" className="app-field"/>
                                        <label htmlFor="appAndroid" className="label-app">Pink на Android</label>
                                </div>
                                <div className="form-row">
                                    <input type="radio" name="app" value="iOS" id="appWin" className="app-field"/>
                                        <label htmlFor="appWin" className="label-app">Windows Phone</label>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div className="form-section-2">
                        <fieldset>
                            <legend>Контактная информация</legend>
                            <div className="form-row first-row">
                                <input type="tel" name="phoneNumber" id="phoneNumber" placeholder="Ваш номер телефона"/>
                                    <p><label htmlFor="phoneNumber">номер телефона</label></p>
                            </div>
                            <div className="form-row second-row">
                                <input type="email" name="email" id="email" placeholder="Ваш email *" required/>
                                    <p><label htmlFor="email">Адрес почты</label></p>
                            </div>
                        </fieldset>
                    </div>
                    <div className="form-section-3">
                        <fieldset>
                            <legend>Опишите свои эмоции</legend>
                            <textarea name="" id=""
                                      placeholder="Можно прям в красках, не стесняясь в выражениях"/>
                        </fieldset>
                    </div>
                    <div className="form-button">
                        <button type="submit" className="main-btn">Отправить форму</button>
                        <p><span>*</span> — обязательные для заполнения поля</p>
                    </div>
                </form>
            </div>
        </div>
        <footer className="footer-form">
            <div className="container footer-inner">
                <p>© Copyright Name Studio</p>
            </div>
        </footer>
        <button className="back" onClick={back}>Back</button>
    </>
)

export default FormPage;