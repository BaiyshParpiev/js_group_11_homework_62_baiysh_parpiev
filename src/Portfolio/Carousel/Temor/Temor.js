import React from 'react';
import './Temor.css';
import imgFirst from './images/img-1.jpg';
import imgSecond from './images/img-2.jpg';
import imgThird from './images/img-3.jpg';

const Temor = ({back}) => (
    <>
        <header className="header-temor">
            <div className="container">
                <div className="header-temor__top">
                    <div className="header-temor__logo">
                        <a href="#" className="temor-logo">Site name</a>
                    </div>
                    <div className="header-temor__nav">
                        <nav className="nav">
                            <ul className="nav-temor__list">
                                <li className="nav__item"><a href="#" className="nav__links">Home</a></li>
                                <li className="nav__item"><a href="#" className="nav__links">About</a></li>
                                <li className="nav__item"><a href="#" className="nav__links">Work</a></li>
                                <li className="nav__item"><a href="#" className="nav__links">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div className="header-bottom-temor">
                    <h1 className="header-bottom__title">Cras ornare trist lorem ipsum <span
                        className="header-bottom__subtitle">cusce pellentesque susc</span></h1>
                </div>
            </div>
        </header>
        <div className="main-block-temor">
            <div className="container">
                <div className="content-top row">
                    <div className="col-12 col-sm-6 content-top__col content-top__red">
                        <div className="title-block"><h2 className="title-block__title ic-mail content-top_ic-position">
                            <a href="#" className="title-block__link">Vestibulum auctor</a></h2></div>
                        <p className="content-top__text">Praesent dapibus, neque id cursus faucibus, tortor neque
                            egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat</p>
                    </div>
                    <div className="col-12 col-sm-6 content-top__col content-top__yellow">
                        <div className="title-block">
                            <h2 className="title-block__title ic-pen content-top_ic-position"><a href="#" className="title-block__link">Integer
                                vitae lib</a></h2>
                        </div>
                        <p className="content-top__text">Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat</p>
                    </div>
                    <div className="col-12 col-sm-6 content-top__col content-top__bluelight">
                        <div className="title-block">
                            <h2 className="title-block__title ic-crown content-top_ic-position"><a href="#" className="title-block__link">tempor
                                interdum</a></h2>
                        </div>
                        <p className="content-top__text">Sed adipiscing ornare risus. Morbi est est, blandit sit amet,
                            sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo
                            ullamcorper</p>
                    </div>
                    <div className="col-12 col-sm-6 content-top__col content-top__pink">
                        <div className="title-block">
                            <h2 className="title-block__title ic-man content-top_ic-position"><a href="#"
                                                                                                 className="title-block__link">felis
                                quis tortor</a></h2>
                        </div>
                        <p className="content-top__text">Sed adipiscing ornare risus. Morbi est est, blandit sit amet,
                            sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo
                            ullamcorper</p>
                    </div>
                </div>
                <div className="content-bottom-temor">
                    <h2 className="content-bottom__title">tempor interdum</h2>
                    <h4 className="content-bottom__subtitle">Integer vitae libero ac risus egestas placerat</h4>
                    <div className="row">
                        <div className="content-bottom__col col-12 col-sm-6 col-md-4">
                            <div className="card-temor card_red">
                                <a href="#" className="card__link-img"><img src={imgFirst} alt=""/></a>
                                <p className="card__info">Cing ornare risus. Morbi est est, binto sinlandit sit amet,
                                    sagittis vel, euismod vel, velit. Pellentesque egestas sem Suspendisse commodo
                                    ullamcorper</p>
                                <a href="#" className="card__button">read more</a>
                            </div>
                        </div>
                        <div className="content-bottom__col col-12 col-sm-6 col-md-4">
                            <div className="card card_yellow">
                                <a href="#" className="card__link-img"><img src={imgSecond} alt=""/></a>
                                <p className="card__info">Joren ornare risus. Morbi est est, binto sin landit sit amet,
                                    sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo
                                    ullamcorper</p>
                                <a href="#" className="card__button">read more</a>
                            </div>
                        </div>
                        <div className="content-bottom__col col-12 col-sm-6 col-md-4">
                            <div className="card card_bluelight">
                                <a href="#" className="card__link-img"><img src={imgThird} alt=""/></a>
                                <p className="card__info">Lorem ornare risus. Morbi est est, binto sin landit sit amet,
                                    sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo
                                    ullamcorper</p>
                                <a href="#" className="card__button">read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="connect-block">
                    <div className="title">
                        <div className="title-block"><h2 className="title-block__title connect-block__ic"><a href="#"
                                                                                                             className="title-block__link">connnect
                            with us</a></h2></div>
                    </div>
                    <h3 className="connect-block__subtitle">Vivamus vestibulum nulla nec ante</h3>
                    <div className="form-block">
                        <form>
                            <div className="form-block__top row">
                                <div className="form-block__name col-12 col-md-6">
                                    <input type="text" name="name" id="name" placeholder="Enter your name"
                                           className="form-block__nametext"/>
                                </div>
                                <div className="form-block__phone col-12 col-md-6">
                                    <input type="text" name="phoneNumber" id="phoneNumber"
                                           placeholder="Enter your phone" className="form-block__phonenumber"/>
                                </div>
                            </div>
                            <div className="form-block__bottom ">
                                <textarea name="message" id="" placeholder="Enter your message"
                                          className="form-block__message"/>
                            </div>
                            <button type="submit" className="form-block__button">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer className="footer-temor">
            <div className="container">
                <div className="footer__top row">
                    <div className="footer__mail col-12 col-sm-2">
                        <a href="#" className="info">info@sitename.com</a>
                    </div>
                    <div className="footer__smedia col-12 col-sm-5">
                        <div className="social--media">
                            <ul className="social-media__list">
                                <li className="social-media__item"><a href="#" className="ic-fb social-media__link"/></li>
                                <li className="social-media__item"><a href="#" className="ic-tw social-media__link"/></li>
                            </ul>
                        </div>
                    </div>
                    <div className="footer__phone col-12 col-sm-5">
                        <a href="#" className="info">(000)777 777 7777</a>
                        <span className="footer__border"/>
                    </div>
                </div>
                <div className="footer__bottom">
                    <p className="web-info">Copyright(c) website name. <span className="web-info__text">Designed by: www.alltemplateneeds.com</span> /
                        Images from: www.wallpaperswide.com, www.photorack.net</p>
                </div>
            </div>
        </footer>
        <button className="back" onClick={back}>Back</button>
    </>
)

export default Temor;