import React from 'react';
import './HomeBuilder.css';
import Message from "../Message/Message";
import Priority from "./Priority/Priority";

const HomeBuilder = () => {
    return (
        <div className="home">
            <div className="home__header-bottom">
                <Message/>
            </div>
            <div className="home__priority">
                <h3 className="home__priority_header">My priorities</h3>
                <Priority/>
            </div>
        </div>
    );
};

export default HomeBuilder;