import React from 'react';
import './Priority.css'
import iconFirst   from '../../../images/drafting-compass-solid.svg';
import iconSecond   from '../../../images/free-code-camp-brands.svg';
import iconThird   from '../../../images/tachometer-alt-solid.svg';
import iconFourth   from '../../../images/window-restore-solid.svg';

const Priority = () => {
    return (
        <div className="priority">
            <div className="priority__item">
                <h4 className="priority__item-header"><span className="d16 first-icon"><img src={iconThird}  alt="icon"/></span>Fast</h4>
                <p className="priority__item-text">Fast load times and lag free interaction, my highest priority.</p>
            </div>
            <div className="priority__item">
                <h4 className="priority__item-header"> <span className="d16 second-icon"><img src={iconFourth} alt="icon"/></span>Responsive</h4>
                <p className="priority__item-text">My layouts will work on any device, big or small.</p>
            </div>
            <div className="priority__item">
                <h4 className="priority__item-header"><span className="d16"><img src={iconSecond} alt="icon"/></span>Intuitive</h4>
                <p className="priority__item-text">Strong preference for easy to use, intuitive UX/UI.</p>
            </div>
            <div className="priority__item">
                <h4 className="priority__item-header"><span className="d16"><img src={iconFirst} alt="icon"/></span>Dynamic</h4>
                <p className="priority__item-text">Websites don't have to be static, I love making pages come to life.</p>
            </div>
        </div>
    );
};

export default Priority;