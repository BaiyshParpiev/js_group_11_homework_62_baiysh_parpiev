import React from 'react';
import '../../Home/HomeBuilder/Priority/Priority.css';
import iconFirst from '../../images/drafting-compass-solid.svg'
import iconSecond from '../../images/free-code-camp-brands.svg';
import iconThird from '../../images/tachometer-alt-solid.svg';
import iconFourth from '../../images/window-restore-solid.svg';
import './ServicesBuilder.css'

const ServicesBuilder = () => {
    return (
        <div className="serviceBuilder">
            <div className="service-animation">
                <div className="system">
                    <div className="system__orbit system__orbit--sun">
                        <img src="https://www.dropbox.com/s/g02pto204mz1ywi/sun.png?raw=1" alt="Sun"
                             className="system__icon system__icon--sun"/>
                    </div>

                    <div className="system__orbit system__orbit--mercury">
                        <div className="system__planet system__planet--mercury">
                            <img src="https://www.dropbox.com/s/2o38602cmwhhdi1/mercury.png?raw=1" alt="Mercury"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--venus">
                        <div className="system__planet system__planet--venus">
                            <img src="https://www.dropbox.com/s/wvictuysutiirho/venus.png?raw=1" alt="Venus"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--earth">
                        <div className="system__planet system__planet--earth">
                            <img src="https://www.dropbox.com/s/ropzlyhb1v19l5t/earth.png?raw=1" alt="Earth"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--mars">
                        <div className="system__planet system__planet--mars">
                            <img src="https://www.dropbox.com/s/fa9biyj617n1q30/mars.png?raw=1" alt="Mars"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--jupiter">
                        <div className="system__planet system__planet--jupiter">
                            <img src="https://www.dropbox.com/s/d28oxi2c74zcoqk/jupiter.png?raw=1" alt="Jupiter"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--saturn">
                        <div className="system__planet system__planet--saturn">
                            <img src="https://www.dropbox.com/s/h8pj72v6mmaa0yq/saturn.png?raw=1" alt="Saturn"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--uranus">
                        <div className="system__planet system__planet--uranus">
                            <img src="https://www.dropbox.com/s/du6znsmfos2r4ry/uranus.png?raw=1" alt="Uranus"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--neptune">
                        <div className="system__planet system__planet--neptune">
                            <img src="https://www.dropbox.com/s/170sr7xl6gxpona/neptune.png?raw=1" alt="Neptune"/>
                        </div>
                    </div>
                    <div className="system__orbit system__orbit--pluto">
                        <div className="system__planet system__planet--pluto">
                            <img src="https://www.dropbox.com/s/z7axkafhs887t9b/pluto.png?raw=1" alt="Pluto"/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="priority ser">
                <div className="priority__item blocks-ser">
                    <h4 className="priority__item-header"><span className="d16 first-icon"><img src={iconThird}  alt="icon"/></span>Custom Web Apps</h4>
                    <p className="priority__item-text">We develop dedicated software and applications with an efficient and functional front-end. We rely on proven technology and frameworks.</p>
                </div>
                <div className="priority__item blocks-ser">
                    <h4 className="priority__item-header"> <span className="d16 second-icon"><img src={iconFourth} alt="icon"/></span>UI/UX Design</h4>
                    <p className="priority__item-text">MWe believe that the success of the digital product is a combination of user satisfaction and the achievement of business goals. The visual layer of software is crucial for this.</p>
                </div>
                <div className="priority__item blocks-ser">
                    <h4 className="priority__item-header"><span className="d16"><img src={iconSecond} alt="icon"/></span>Cross-Platform Development</h4>
                    <p className="priority__item-text">Based on cross-platform frameworks and PWA standards, we create applications that provide native experiences on multiple systems simultaneously.</p>
                </div>
                <div className="priority__item blocks-ser">
                    <h4 className="priority__item-header"><span className="d16"><img src={iconFirst} alt="icon"/></span>JavaScript Development</h4>
                    <p className="priority__item-text">We create front-end based on JS frameworks such as Vue.js, React.js, Angular. As javascript specialists also write dedicated solutions from scratch.</p>
                </div>
            </div>
        </div>
);
};

export default ServicesBuilder;

