import React from 'react';
import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import HomeBuilder from "./Home/HomeBuilder/HomeBuilder";
import PortfolioBuilder from "./Portfolio/PortfolioBuilder/PortfolioBuilder";
import ServicesBuilder from "./Services/ServicesBuilder/ServicesBuilder";
import ContactBuilder from "./Contact/ContactBuilder/ContactBuilder";
import Whatsapp from "./images/whatsapp-brands.svg";
import Facebook from "./images/facebook-brands.svg";
import Twitter from "./images/twitter-brands.svg";
import Instagram from "./images/instagram-square-brands.svg";
import Linkedin from "./images/linkedin-brands.svg";
import Bitbucket from "./images/bitbucket-brands.svg";

const App = () => {

  return (
      <BrowserRouter>
        <div className="App">
          <header className="App-header">
            <div className="container header">
              <a href="https://shop.mango.com/" className="header__logo">
                <img src={logo} className="App-logo" alt="logo" />
              </a>
              <div className="header__links">
                <ul>
                  <NavLink to="/">Home</NavLink>
                  <NavLink to="/portfolio">Portfolio</NavLink>
                  <NavLink to="/services">Services</NavLink>
                  <NavLink to="/contact">Contact</NavLink>
                </ul>
              </div>
            </div>
          </header>
          <div className="block container">
            <Switch>
              <Route path="/" exact component={HomeBuilder}/>
              <Route path="/portfolio"  component={PortfolioBuilder}/>
              <Route path="/services" component={ServicesBuilder}/>
              <Route path="/contact" component={ContactBuilder}/>
            </Switch>
          </div>
          <footer className="footer">
            <div className="container">
              <div className="footer__top row">
                <div className="footer__mail ">
                  <a href="#" className="info">baiysh.parpiev@gmail.com</a>
                </div>
                <div className="footer__smedia">
                  <div className="social-media">
                    <ul className="social-media__list">
                      <li className="social-media__item"><a href="https://www.facebook.com/baiysh.parpiev" className="social-media__link"><img src={Facebook} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://www.instagram.com/bayish.parpiev" className=" social-media__link"><img src={Instagram} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://www.linkedin.com/in/bayish-parpiev-0b7534138/" className="social-media__link"><img src={Linkedin} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://t.me/frontenD96" className=" social-media__link"><img src={Whatsapp} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://twitter.com/BayishOfficial" className="social-media__link"><img src={Twitter} alt="link"/></a></li>
                      <li className="social-media__item"><a href="https://bitbucket.org/BaiyshParpiev/" className="social-media__link"><img src={Bitbucket} alt="link"/></a></li>
                    </ul>
                  </div>
                </div>
                <div className="footer__phone">
                  <a href="#" className="info">(+49) 1722962698</a>
                  <span className="footer__border"/>
                </div>
              </div>
              <div className="footer__bottom">
                <p className="web-info">Copyright(c) website name. <span className="web-info__text">Designed by: www.alltemplateneeds.com</span> /
                  Images from: www.wallpaperswide.com, www.photorack.net</p>
              </div>
            </div>
          </footer>
        </div>

      </BrowserRouter>
  )
}



export default App;
